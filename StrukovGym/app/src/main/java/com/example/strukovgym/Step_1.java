package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Step_1 extends AppCompatActivity {
Button bt1,bt2;
TextView step,text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_1);
        bt1 = findViewById(R.id.button);
        bt2 = findViewById(R.id.button2);
        step = findViewById(R.id.textView);
        text = findViewById(R.id.textView2);


    }
    public void onClick (View view){
        Intent in = new Intent(Step_1.this,Step_1_2.class);
        startActivity(in);
        finish();
    }
}
