package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Step_1_2 extends AppCompatActivity {
    Button bt1,bt2,bt3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_1_2);
        bt1 = findViewById(R.id.button);
        bt2 = findViewById(R.id.button2);
        bt3 = findViewById(R.id.button3);


    }

    public void onClick (View view){
        switch (view.getId()){
            case R.id.button:
                bt1.setBackgroundResource(R.drawable.button_yes);
                bt2.setBackgroundResource(R.drawable.buttom_no);
                bt3.setBackgroundResource(R.drawable.buttom_no);
                break;
            case R.id.button2:
                bt1.setBackgroundResource(R.drawable.buttom_no);
                bt2.setBackgroundResource(R.drawable.button_yes);
                bt3.setBackgroundResource(R.drawable.buttom_no);
                break;
            case R.id.button3:
                bt1.setBackgroundResource(R.drawable.buttom_no);
                bt2.setBackgroundResource(R.drawable.buttom_no);
                bt3.setBackgroundResource(R.drawable.button_yes);
                break;

        }
        Intent in = new Intent(Step_1_2.this,Step_2.class);
        startActivity(in);
        finish();
    }
}
