package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postAtTime(new Runnable() {
            @Override
            public void run() {
                Intent in = new Intent(MainActivity.this,Step_1.class);
                startActivity(in);
                finish();
            }
        },2500);
    }
}
